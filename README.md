# Self Assessment (German)

This is the source code I used for Self Assessment at the start of "Nichtsequenzielle und verteile Programmierung" at the Freie Universität Berlin in the Summersemester of 2022.

It allows users to assess with the help of a slider how competent they are in the C Programming Language and how much they know about git. The website stores a cookie with a unique user id which will be stored in a database alongside with results to enable editing results but also keep anonymity.
