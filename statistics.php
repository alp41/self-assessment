<?php
	$server = "";
	$username = "";
	$password = "";
	$database = "";

	$conn = new mysqli($server, $username, $password, $database);

	if($conn->connect_error) {
		die("err");
	}

	$query = "SELECT c, git FROM skills";
	$result = $conn->query($query);
	header('Content-Type: application/json');
	echo json_encode($result->fetch_all(MYSQLI_ASSOC));
?>