<?php
	$uuid = $_GET['uuid'];
	//Generates UUID that does not exist in the database yet
	$server = "";
	$username = "";
	$password = "";
	$database = "";

	$conn = new mysqli($server, $username, $password, $database);

	if($conn->connect_error) {
		die("err");
	}
	if(isset($_GET['uuid']) && doesUUIDExist($conn, $_GET['uuid'])) {
		echo($_GET['uuid']);
		die();
	}
	$userId = gen($conn);

	while(doesUUIDExist($conn, $userId)) {
		$userId = gen($conn);
	}

	$stmt = $conn->prepare("INSERT INTO skills (uuid, c, git) VALUES (?, NULL, NULL)");
	$stmt->bind_param("s", $userId);
	$stmt->execute();
	echo $userId;
	

	function doesUUIDExist($conn, $uid) {
		$stmt = $conn->prepare("SELECT * FROM skills WHERE uuid = ?");
		$stmt->bind_param("s", $uid);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->num_rows > 0;
	}


	function gen($conn) {
		$sql = "SELECT UUID();";
		$result = $conn->query($sql);

		if($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$uuid = $row["UUID()"];
		}
		return $uuid;
	}
?>