<?php
	$uuid = $_POST["uuid"];
	$c = $_POST["c"];
	$git = $_POST["git"];

	$server = "";
	$username = "";
	$password = "";
	$database = "";

	$conn = new mysqli($server, $username, $password, $database);

	if($conn->connect_error) {
		die("err");
	}

	$stmt = $conn->prepare("UPDATE skills SET c = ?, git = ? WHERE uuid = ?");
	$stmt->bind_param("iis", $c, $git, $uuid);

	$stmt->execute();

	echo "1";
?>